*** Settings ***
Documentation    ✅ BDD auto CT
Metadata         ID                           8
Metadata         Automation priority          null
Metadata         Test case importance         Low
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
✅ BDD auto CT
    [Documentation]    ✅ BDD auto CT

    Given a given


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the ${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the ${TEST_8_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, ${TEST_8_SETUP} will be run after ${TEST_SETUP}.

    ${TEST_SETUP_VALUE} =      Get Variable Value    ${TEST_SETUP}
    ${TEST_8_SETUP_VALUE} =    Get Variable Value    ${TEST_8_SETUP}
    IF    $TEST_SETUP_VALUE is not None
        Run Keyword    ${TEST_SETUP}
    END
    IF    $TEST_8_SETUP_VALUE is not None
        Run Keyword    ${TEST_8_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the ${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the ${TEST_8_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, ${TEST_TEARDOWN} will be run after ${TEST_8_TEARDOWN}.

    ${TEST_8_TEARDOWN_VALUE} =    Get Variable Value    ${TEST_8_TEARDOWN}
    ${TEST_TEARDOWN_VALUE} =      Get Variable Value    ${TEST_TEARDOWN}
    IF    $TEST_8_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_8_TEARDOWN}
    END
    IF    $TEST_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_TEARDOWN}
    END
