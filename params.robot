*** Settings ***
Library          squash_tf.TFParamService


*** Test Cases ***
Get Params Test
    ${param1} =    Get Test Param    DS_param1
    ${param2} =    Get Test Param    DS_param2
    ${param3} =    Get Test Param    DS_param3
    ${param4} =    Get Test Param    DS_param4
    ${param5} =    Get Test Param    DS_param5
    ${param6} =    Get Test Param    DS_param6

    Log To Console    Les params ${\n}*${param1}*${\n}*${param2}*${\n}*${param3}*${\n}*${param4}*${\n}*${param5}*${\n}*${param6}*
