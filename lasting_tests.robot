*** Test Cases ***
The KO Test
    Fail    I am a KO test, Thus, I Fail.

The other KO Test
    Run Keyword And Continue On Failure    Fail But Continue
    Fail    I fail and stop

3 Seconds Wait
    The Test During Time    3 s

30 Seconds Wait
    The Test During Time    30 s

2 Minutes Wait
    The Test During Time    2 minutes

1 Minutes Wait
    The Test During Time    1 minutes

3 Minutes Wait
    The Test During Time    3 minutes

5 Minutes Wait
    The Test During Time   5 minutes

7 Minutes Wait
    The Test During Time    7 minutes

10 Minutes Wait
    The Test During Time    10 minutes

15 Minutes Wait
    The Test During Time    15 minutes

20 Minutes Wait
    The Test During Time    20 minutes

25 Minutes Wait
    The Test During Time    25 minutes

30 Minutes Wait
    The Test During Time    30 minutes

40 Minutes Wait
    The Test During Time    40 minutes

45 Minutes Wait
    The Test During Time    45 minutes

60 Minutes Wait
    The Test During Time    60 minutes


*** Keywords ***
The Test During Time
    [Arguments]    ${time}
    Log To Console    Test ${TEST_NAME} starts...
    Sleep    ${time}
    Log To Console    Test ${TEST_NAME} done.

Fail But Continue
    ${LONG_ERROR_MSG} =    Set Variable    
    ...    AssertionError: ❌ Test FAILED at step: [Verify calculation output].
    ...    Expected: 🌟 "42 * 100 = 4200 🎉🎉🎉"
    ...    Found: "42 * 100 = 4199 😱"
    ...    --- Detailed Debug Logs ---
    ...    ➡️ STEP 1: Input received: [a=42, b=100] 🚀
    ...    ➡️ STEP 2: Applied function: multiply() 🧮
    ...    ➡️ STEP 3: Intermediary values: result=[4199.99999999] (rounding issue?) 🤔
    ...    ➡️ STEP 4: Compared to baseline: [baseline=4200, tolerance=0.01] ⚠️
    ...    --- Additional Notes ---
    ...    🔍 Root Cause Analysis: Potential overflow in calculation logic due to incorrect rounding 🌀.
    ...    🛠 Suggested Fix: Update rounding algorithm to handle precision with DOUBLE data types ⚙️.
    ...    🐞 StackTrace:
    ...    • CalculatorLibrary.py line 42: result = multiply(42, 100)
    ...    • AssertionFailed: raise Exception("Expected 4200, but got 4199")
    ...    --- END OF ERROR REPORT ---
    Fail    ${LONG_ERROR_MSG}