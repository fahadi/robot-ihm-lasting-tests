*** Test Cases ***
The KO Test
    Fail    Test failed, 2 != 4


The KO Test 2
    Fail     ${Unknown variable}

The KO Test 3
    ${LONG_ERROR_MSG} =    Set Variable    
    ...    🔍 Root Cause Analysis: Potential overflow in calculation logic due to incorrect rounding 🌀. 🛠 Suggested Fix: Update rounding algorithm to handle precision with DOUBLE data types ⚙️. Expected: 🌟 "42 * 100 = 4200 🎉🎉🎉"
    ...    Found: "42 * 100 = 4199 😱"
    ...    --- Detailed Debug Logs ---
    ...    ➡️ STEP 1: Input received: [a=42, b=100] 🚀
    ...    ➡️ STEP 2: Applied function: multiply() 🧮
    ...    ➡️ STEP 3: Intermediary values: result=[4199.99999999] (rounding issue?) 🤔
    ...    ➡️ STEP 4: Compared to baseline: [baseline=4200, tolerance=0.01] ⚠️
    ...    --- Additional Notes ---
    ...    🔍 Root Cause Analysis: Potential overflow in calculation logic due to incorrect rounding 🌀.
    ...    🛠 Suggested Fix: Update rounding algorithm to handle precision with DOUBLE data types ⚙️.
    ...    🐞 StackTrace:
    ...    • CalculatorLibrary.py line 42: result = multiply(42, 100)
    ...    • AssertionFailed: raise Exception("Expected 4200, but got 4199")
    ...    --- END OF ERROR REPORT ---
    Fail     ${LONG_ERROR_MSG}

The continue on failure Test
    Run Keyword And Continue On Failure        Should Be Equal    2    8
    Run Keyword And Continue On Failure    I don't exist
    Fail    I fail and stop
