*** Settings ***
Library    String


*** Test Cases ***
5 Minutes Str
    Repeat Keyword    5 m    Get Length Random String

15 Minutes Str
    Repeat Keyword    15 m    Get Length Random String

30 Minutes Str
    Repeat Keyword    30 m    Get Length Random String

60 Minutes Str
    Repeat Keyword    60 m    Get Length Random String

120 Minutes Str
    Repeat Keyword    120 m    Get Length Random String

5 Minutes Maths
    Repeat Keyword    5 min    Do Maths

10 Minutes Maths
    Repeat Keyword    10 m    Do Maths

15 Minutes Maths
    Repeat Keyword    15 m    Do Maths

20 Minutes Maths
    Repeat Keyword    20 m    Do Maths

30 Minutes Maths
    Repeat Keyword    30 m    Do Maths

45 Minutes Maths
    Repeat Keyword    45 m    Do Maths


*** Keywords ***
Get Length Random String
    ${random_int}=    Evaluate    random.randint(0,10)    random
    ${random_str}=    Generate Random String    length=${random_int}
    ${str_lenght}=    Get Length    ${random_str}
    Log To Console    str_length is ${str_lenght}

Do Maths
    ${random_int_1}=    Evaluate    random.randint(0,100)    random
    ${random_int_2}=    Evaluate    random.randint(0,500)    random
    ${result_add}=    Evaluate    int(${random_int_1}) + int(${random_int_2})
    ${result_multiply}=    Evaluate    int(${random_int_1}) * int(${random_int_2})
    Log To Console    Addition : ${random_int_1} + ${random_int_2} = ${result_add}
    Log To Console    Multiplication : ${random_int_1} * ${random_int_2} = ${result_multiply}