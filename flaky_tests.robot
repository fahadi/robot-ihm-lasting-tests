*** Test Cases ***
The Flaky Test
    I fail if the number is odd

The Flaky Test 2
    I fail if the number is odd

*** Keywords ***
I fail if the number is odd
    ${number} =    Evaluate    random.randrange(1,9)    random
    ${remainder}=    Evaluate    ${number} % 2
    IF    '${remainder}' == '0'
        Pass Execution   Number is even
    ELSE
        Fail    Number ${number} is odd !
    END 
